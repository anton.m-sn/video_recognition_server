from flask import Blueprint, jsonify, request
from flask_restful import Api, Resource, abort


class VideoAPI(Resource):

    def post(self):
        file = request.files['upload_file']
        file_contents = file.stream.read()
        from src.tasks import detect_video_task
        result = detect_video_task.delay(file_contents)
        result.get()
        if result.successful():
            return jsonify(result.result)
        else:
            abort(500)


def register_routes(_app):
    api_blueprint = Blueprint("api", __name__)
    api = Api(api_blueprint, catch_all_404s=False)

    api.add_resource(VideoAPI, "/video", strict_slashes=False)

    _app.register_blueprint(api_blueprint, url_prefix="/api")
