from celery import Celery


def make_celery():
    celery_app = Celery('src')
    celery_app.config_from_object('src.celery_config')

    return celery_app


celery = make_celery()
