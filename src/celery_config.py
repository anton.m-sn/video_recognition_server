broker_url = 'pyamqp://'
result_backend = 'redis://localhost'

task_serializer = 'pickle'
result_serializer = 'pickle'
accept_content = ['pickle']
timezone = 'UTC'

imports = ('src.tasks', )
