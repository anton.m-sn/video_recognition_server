import requests


if __name__ == '__main__':
    url = 'http://localhost:5000/api/video'
    filename = '../video.mp4'
    with open(filename, 'rb') as video_file:
        files = {'upload_file': video_file}
        response = requests.post(url, files=files)
        print(response.json())
