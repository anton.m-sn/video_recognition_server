import os
import uuid
from typing import List, Tuple

from src.celery_app import celery
from src.image_processing import VideoDetector


@celery.task()
def detect_video_task(file_contents: bytes) -> List[List[Tuple[str, float]]]:
    file_path = f'./file_storage/{uuid.uuid4()}.mp4'
    with open(file_path, 'wb') as file_out:
        file_out.write(file_contents)
    detection_result = VideoDetector().detect_video(file_path)
    os.remove(file_path)

    return detection_result
