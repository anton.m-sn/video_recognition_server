from typing import List, Tuple

import cv2
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog
from detectron2.engine import DefaultPredictor
from detectron2.utils.logger import setup_logger
from numpy.typing import NDArray

setup_logger()


class VideoDetector:

    def __init__(self):
        self._image_detector = ImageDetector()

    def detect_video(self, filename: str) -> List[List[Tuple[str, float]]]:
        video_capture = cv2.VideoCapture(filename)
        result = []
        while True:
            _, frame = video_capture.read()
            if frame is None:
                break
            frame_result = self._image_detector.infer_image(frame)
            result.append(frame_result)
        video_capture.release()

        return result


class ImageDetector:

    def __init__(self):
        self._config = self._get_config()
        self._metadata = MetadataCatalog.get(self._config.DATASETS.TRAIN[0])
        self._class_catalog = self._metadata.thing_classes
        self._predictor = DefaultPredictor(self._config)

    @staticmethod
    def _get_config():
        config = get_cfg()

        # add project-specific config (e.g., TensorMask) here if you're not running a model in detectron2's core library
        config.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
        config.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model

        # Find a model from detectron2's model zoo. You can use the https://dl.fbaipublicfiles... url as well
        config.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")
        config.MODEL.DEVICE = 'cpu'

        return config

    def infer_image(self, image: NDArray) -> List[Tuple[str, float]]:
        """Detects objects in a given image.

        Args:
            image: The image to infer

        Returns:
            List of tuples (object_class_name, confidence_score)

        """
        outputs = self._predictor(image)
        instances = outputs["instances"]
        detected_class_indexes = instances.pred_classes
        confidence_scores = instances.scores

        result = []
        for idx, score in enumerate(confidence_scores):
            class_index = detected_class_indexes[idx]
            class_name = self._class_catalog[class_index]
            result.append((class_name, score.item()))

        return result
