# Video recognition server

It accepts mp4 video files and returns detected objects for each video frame. Each video is processed as a celery task (possibly on a separate machine). Detectron2 runs inference in parallel (using CPU cores or GPUs). Current setting is to use CPU, which is very slow (~2.5 seconds per frame). Using GPU can improve inference speed dramatically.

##### Technology used:
* Python 3.8
* OpenCV for video processing
* Detectron2 for image recognition
* Flask-Restful for web server
* Celery for task distribution
    * Redis result backend
    * RabbitMQ message broker

## Architecture
The code is ready to be used in a distributed system, but config changes are necessary.

The web server should be run on Gunicorn with Gevent or Eventlet workers.
![](architecture.png)
